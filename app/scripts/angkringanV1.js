var item = [
	{nama:'ati',tulisan:'Sate Ati',harga :5000,gmbr:'/gambar/ati.svg'},
	{nama:'gorengan',tulisan:'Gorengan',harga: 1000,gmbr:'/gambar/gorengan1.svg'},
	{nama:'mie',tulisan:'Mie',harga: 5000,gmbr:'/gambar/mie.svg'},
	{nama:'nasi',tulisan:'Nasi Kucing',harga:2500,gmbr:'/gambar/nasikucing.svg'},
	{nama:'sateBaso',tulisan:'Sate Baso',harga:2500,gmbr:'/gambar/satebaso.svg'},
	{nama:'lemonTea',tulisan:'Lemon Tea',harga:3000,gmbr:'/gambar/eslemontea.svg'},
	{nama:'kopi',tulisan:'Kopi',harga:3000,gmbr:'/gambar/hotcoffe.svg'},
	{nama:'esjeruk',tulisan:'Es Jeruk',harga:3000,gmbr:'/gambar/juice.svg'},
	{nama:'susu',tulisan:'Susu',harga:3000,gmbr:'/gambar/milk.svg'},
	{nama:'teh',tulisan:'Teh Manis',harga:2000,gmbr:'/gambar/tea.svg'}
];

var itemBeli = []
var itemIDX = 0
var jmlpesan = 1;
function popati(idx) {
	// Cek menu yang dipilih sudah pernah di pesan
	if (itemBeli.length>0){
		for (var i in itemBeli) {
			if (itemBeli[i].idxBeli == idx){
				jmlpesan = itemBeli[i].jmlBeli;
			}
		}
	}else{
		// menu belum pernah dipesan
		jmlpesan = 1;
	}

	document.getElementById('popup').style.display = 'block';
	document.getElementById('popjudul').innerHTML = item[idx].tulisan;
	document.getElementById('popimg').innerHTML = '<img src=\"'+item[idx].gmbr+'\">'
	document.getElementById('pophrg').innerHTML = 'Rp '+item[idx].harga;
	document.getElementById('jpesan').innerHTML = jmlpesan;
	itemIDX = idx;
	
}

function tblTambah() {
	jmlpesan++;
	document.getElementById('jpesan').innerHTML = jmlpesan;
	// console.log('tes',jmlpesan);
}
function tblKurang() {
	if (jmlpesan>0){
		jmlpesan--;
		document.getElementById('jpesan').innerHTML = jmlpesan;
		// console.log('tes',jmlpesan);
	}
}
var totalBayar = 0
function tblTutup() {
		var editBeli = false;
		if (itemBeli.length>0) {
			for (var i in itemBeli) {
				if (itemBeli[i].idxBeli==itemIDX) {
					editBeli = true;
					break;
				}
			}
		}

	if (jmlpesan>0){
		
			// jika sudah pernah pesan EDIT jmlbeli
			// jika belum PUSH
		

		// EDIT atau PUSH itemBeli
		if (editBeli) {
			console.log('edit data');
			itemBeli[i].jmlBeli=jmlpesan;
		} else {
			console.log('data baru');
			itemBeli.push({idxBeli:itemIDX,jmlBeli:jmlpesan});	
		};

		//================
		// tampil di tabel #list-beli
		// Delete Row #list-beli
		$("#list-beli").children().remove();
		
		var tabel = document.getElementById('list-beli');
		totalBayar = 0;

		for (var x in itemBeli) {
			// console.log('itemBeli index=',itemBeli[x].idxBeli);
			var row = tabel.insertRow();
			var cItem = row.insertCell(0);
			var cJml = row.insertCell(1);
			var cHrg = row.insertCell(2);
			var cSubT = row.insertCell(3);
			cItem.innerHTML = item[itemBeli[x].idxBeli].tulisan;
			cJml.innerHTML = itemBeli[x].jmlBeli;
			cHrg.innerHTML = item[itemBeli[x].idxBeli].harga;
			var subTotal = item[itemBeli[x].idxBeli].harga*itemBeli[x].jmlBeli;
			cSubT.innerHTML = subTotal;
			totalBayar = totalBayar +subTotal;
		}

		// Total beli
		document.getElementById('total-beli').innerHTML = 'Rp. '+totalBayar;

	} else if (jmlpesan==0) {
		// Batal Pesan = Jumlah jadi 0
		if (editBeli) {
			console.log('edit data x');
			console.log(i,'test hapus pesanan ',item[itemBeli[i].idxBeli].tulisan);
			itemBeli.splice(i,1);

			// Update Tabel
			$("#list-beli").children().remove();
			var tabel = document.getElementById('list-beli');
			totalBayar = 0;

			for (var x in itemBeli) {
				var row = tabel.insertRow();
				var cItem = row.insertCell(0);
				var cJml = row.insertCell(1);
				var cHrg = row.insertCell(2);
				var cSubT = row.insertCell(3);
				cItem.innerHTML = item[itemBeli[x].idxBeli].tulisan;
				cJml.innerHTML = itemBeli[x].jmlBeli;
				cHrg.innerHTML = item[itemBeli[x].idxBeli].harga;
				var subTotal = item[itemBeli[x].idxBeli].harga*itemBeli[x].jmlBeli;
				cSubT.innerHTML = subTotal;
				totalBayar = totalBayar +subTotal;
			}

			// Total beli
			document.getElementById('total-beli').innerHTML = 'Rp. '+totalBayar;
		};
	}

	console.log('test', itemBeli);
	document.getElementById('popup').style.display = 'none';
	jmlpesan = 1;
}
function tblBatal () {
	document.getElementById('popup').style.display = 'none';
	jmlpesan = 1;
}

function batalSemua () {
	// document.getElementById('item-beli').innerHTML = "";
	// document.getElementById('jumlah-beli').innerHTML = "";
	// document.getElementById('harga-beli').innerHTML = "";
	// document.getElementById('sub-beli').innerHTML = "";
	document.getElementById('total-beli').innerHTML = "";
	document.getElementById('list-beli').innerHTML = "";
	totalBayar = 0
}

function bayar() {
	document.getElementById('total-bayar').innerHTML = 'Rp. '+totalBayar;
}

function kembalian() {
	var uang = document.getElementById('uang-bayar').value;
	if (uang>totalBayar){
	var kembali = uang - totalBayar;
	console.log(kembali);
	document.getElementById('uangKembali').innerHTML = 'Uang Kembalian Rp. '+kembali;
	} else {
	document.getElementById('uangKembali').innerHTML = 'Uang Anda Kurang';
	}
}
