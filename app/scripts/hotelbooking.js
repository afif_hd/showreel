var roomType = [
	{kode:2,tipe:'Superior',tarif:250000},
	{kode:3,tipe:'Deluxe',tarif:400000},
	{kode:4,tipe:'Suite',tarif:850000},
	{kode:5,tipe:'Presidential Suite',tarif:1200000}];

var allRoom = [
	{room_no:201,tipe:'Superior',tarif:250000,r_status:false},
	{room_no:202,tipe:'Superior',tarif:250000,r_status:true},
	{room_no:203,tipe:'Superior',tarif:250000,r_status:true},
	{room_no:204,tipe:'Superior',tarif:250000,r_status:false},
	{room_no:301,tipe:'Superior',tarif:250000,r_status:false},
	{room_no:302,tipe:'Superior',tarif:250000,r_status:true},
	{room_no:303,tipe:'Superior',tarif:250000,r_status:true},
	{room_no:304,tipe:'Superior',tarif:250000,r_status:false},
	{room_no:401,tipe:'Deluxe',tarif:400000,r_status:true},
	{room_no:402,tipe:'Deluxe',tarif:400000,r_status:false},
	{room_no:403,tipe:'Deluxe',tarif:400000,r_status:true},
	{room_no:501,tipe:'Deluxe',tarif:400000,r_status:false},
	{room_no:502,tipe:'Deluxe',tarif:400000,r_status:false},
	{room_no:503,tipe:'Deluxe',tarif:400000,r_status:true},
	{room_no:601,tipe:'Suite',tarif:850000,r_status:true},
	{room_no:602,tipe:'Suite',tarif:850000,r_status:false},
	{room_no:701,tipe:'Suite',tarif:850000,r_status:true},
	{room_no:702,tipe:'Suite',tarif:850000,r_status:false},
	{room_no:801,tipe:'Presidential Suite',tarif:1200000,r_status:false},
	{room_no:901,tipe:'Presidential Suite',tarif:1200000,r_status:true}];

$('#roomlist').html('');
for (var i in allRoom) {
	if (allRoom[i].r_status) {
		var dvclass = 'row Rada';
		var ket = 'Available';
	} else {
		var dvclass = 'row Rbook';
		var ket = 'Booked';
	}

	var kamar = '<div class="'+dvclass+'">'+'<div class="col-3n">'+allRoom[i].room_no+'</div>'+'<div class="col-5n">'+allRoom[i].tipe+'</div>'+'<div class="col-4n">Rp. '+allRoom[i].tarif+'</div>'+'<div class="col-3n">'+ket+'</div>'+'</div>';

	$('#roomlist').append(kamar);
}

var tglsekarang = new Date();
for (var i = 0; i <= 5; i++) {
	var newOption = document.createElement('option')
	newOption.value = tglsekarang.getFullYear()+i;
	newOption.text = tglsekarang.getFullYear()+i;
	document.getElementById('thnCi').append(newOption);
}
var txBln = [
	{nam:'Januari',nm:1},
	{nam:'Februari',nm:2},
	{nam:'Maret',nm:3},
	{nam:'April',nm:4},
	{nam:'Mei',nm:5},
	{nam:'Juni',nm:6},
	{nam:'Juli',nm:7},
	{nam:'Agustus',nm:8},
	{nam:'September',nm:9},
	{nam:'Oktober',nm:10},
	{nam:'November',nm:11},
	{nam:'Desember',nm:12}
];

for (var i = 0; i < txBln.length; i++) {
	var newOption = document.createElement('option')
	newOption.value = txBln[i].nm;
	newOption.text = txBln[i].nam;
	document.getElementById('blnCi').append(newOption);
}
document.getElementById('blnCi').options.selectedIndex = tglsekarang.getMonth();

for (var i = 1; i <=31; i++) {
	var newOption = document.createElement('option')
	newOption.value = i;
	newOption.text = i;
	document.getElementById('tglCi').append(newOption);
}
document.getElementById('tglCi').options.selectedIndex = tglsekarang.getDate()-1;