$('#sw-TR').click(function () {$('#lapPage').hide('slow');});
$('#sw-LP').click(function () {$('#lapPage').show('slow');});

var daftarMenu = [
	{id:0,nama:'TOFFEE CARAMEL',hrg:31000,img:'/img/angkringan/743.jpg'},
	{id:1,nama:'BLUEBERRY CHEESE CAKE',hrg:31000,img:'/img/angkringan/559.jpg'},
	{id:2,nama:'CHOCOLATE BROWNIE CHEESE',hrg:25000,img:'/img/angkringan/565.jpg'},
	{id:3,nama:'CHOCOLATE DEVIL',hrg:29000,img:'/img/angkringan/746.jpg'},
	{id:4,nama:'CHOCOLATE CRUNCHY',hrg:27000,img:'/img/angkringan/747.jpg'},
	{id:5,nama:'MADELINE ROSE',hrg:30000,img:'/img/angkringan/847.jpg'},
	{id:6,nama:'VANILLA FRUITS',hrg:28000,img:'/img/angkringan/748.jpg'},
	{id:7,nama:'BLACK FOREST',hrg:28000,img:'/img/angkringan/749.jpg'},
	{id:8,nama:'CHOCOLATE TIRAMISU',hrg:27000,img:'/img/angkringan/750.jpg'},
	{id:9,nama:'STRAWBERRY CHEESE CAKE',hrg:30000,img:'/img/angkringan/745.jpg'},
	{id:10,nama:'TIRAMISU',hrg:30000,img:'/img/angkringan/751.jpg'},
	{id:11,nama:'CHOCOLATE EMOTION',hrg:27000,img:'/img/angkringan/754.jpg'},
	{id:12,nama:'TRIPLE CHOCOLATE',hrg:27000,img:'/img/angkringan/755.jpg'},
	{id:13,nama:'CHOCOLATE MOUSSE',hrg:30000,img:'/img/angkringan/813.jpg'},
	{id:14,nama:'AVOCADO GENOA',hrg:27000,img:'/img/angkringan/814.jpg'}
];

for (var menu in daftarMenu) {
	var MenuTampil = '<div class="menu sp3">'+'<div class="mBox" onclick="showPop('+menu+')"><img src="'+daftarMenu[menu].img+'"></div>'+'<label onclick="showPop('+menu+')">'+(daftarMenu[menu].nama).toLowerCase()+'</label></div>'
	$('#listMenu').append(MenuTampil);
}

$('#iClose').click(function () {$('#popup').hide('fast');});
function showPop (idx) {
	$('#popup').show('fast');
	$('#popJudul').html(daftarMenu[idx].nama);
	$('#popHarga').html(daftarMenu[idx].hrg);
	$('#popGambar').html('<img src="'+daftarMenu[idx].img+'">');
}